# click on the desktop to get menus
OnDesktop Mouse1 :HideMenus
OnDesktop Mouse2 :WorkspaceMenu
OnDesktop Mouse3 :RootMenu

# scroll on the desktop to change workspaces
OnDesktop Mouse4 :PrevWorkspace
OnDesktop Mouse5 :NextWorkspace

# scroll on the toolbar to change current window
OnToolbar Mouse4 :PrevWindow {static groups} (iconhidden=no)
OnToolbar Mouse5 :NextWindow {static groups} (iconhidden=no)

# alt + left/right click to move/resize a window
OnWindow Mod1 Mouse1 :MacroCmd {Raise} {Focus} {StartMoving}
OnWindowBorder Move1 :StartMoving

OnWindow Mod1 Mouse3 :MacroCmd {Raise} {Focus} {StartResizing NearestCorner}
OnLeftGrip Move1 :StartResizing bottomleft
OnRightGrip Move1 :StartResizing bottomright

# alt + middle click to lower the window
OnWindow Mod1 Mouse2 :Lower

# control-click a window's titlebar and drag to attach windows
OnTitlebar Control Mouse1 :StartTabbing

# double click on the titlebar to shade
OnTitlebar Double Mouse1 :Shade

# left click on the titlebar to move the window
OnTitlebar Mouse1 :MacroCmd {Raise} {Focus} {ActivateTab}
OnTitlebar Move1  :StartMoving

# middle click on the titlebar to lower
OnTitlebar Mouse2 :Lower

# right click on the titlebar for a menu of options
OnTitlebar Mouse3 :WindowMenu

# alt-tab
Mod4 Tab :NextWindow {groups} (workspace=[current])
Mod4 Shift Tab :PrevWindow {groups} (workspace=[current])

# cycle through tabs in the current window
Mod1 Tab :NextTab
Mod1 Shift Tab :PrevTab

# open a terminal
Mod4 Return :Exec xterm

# open a dialog to run programs
Mod4  space :Exec rofi -show run
#Mod4 space :Exec dmenu_run
#Mod4 space :Exec fbrun -w 400px, -h 40px, -bg "#323d43" -fg "#d8caac"

# volume settings, using common keycodes
# if these don't work, use xev to find out your real keycodes
XF86AudioMute        :Exec amixer set Master toggle
XF86AudioRaiseVolume :Exec pactl set-sink-volume 0 +5%
XF86AudioLowerVolume :Exec pactl set-sink-volume 0 -5%
XF86AudioMicMute     :Exec pactl set-source-mute 1 toggle

# current window commands
Mod4 c       :Close
Mod4 q       :Kill
Mod4 h       :Minimize
Mod4 m       :Maximize
Mod4 f       :Fullscreen

# open the window menu
Mod1 space :WindowMenu

# exit fluxbox
Control Mod1 Delete :Exit

# change to previous/next workspace
Control Mod1 Left :PrevWorkspace
Control Mod1 Right :NextWorkspace

# send the current window to previous/next workspace
Mod4 Left :SendToPrevWorkspace
Mod4 Right :SendToNextWorkspace

# send the current window and follow it to previous/next workspace
Control Mod4 Left :TakeToPrevWorkspace
Control Mod4 Right :TakeToNextWorkspace

# change to a specific workspace
Mod4 1 :Workspace 1
Mod4 2 :Workspace 2
Mod4 3 :Workspace 3
Mod4 4 :Workspace 4

# send the current window to a specific workspace
Mod4 F1 :SendToWorkspace 1
Mod4 F2 :SendToWorkspace 2
Mod4 F3 :SendToWorkspace 3
Mod4 F4 :SendToWorkspace 4

# send the current window and change to a specific workspace
Mod4 shift 1 :TakeToWorkspace 1
Mod4 shift 2 :TakeToWorkspace 2
Mod4 shift 3 :TakeToWorkspace 3
Mod4 shift 4 :TakeToWorkspace 4
