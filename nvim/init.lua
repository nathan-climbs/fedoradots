-- neovim init Oct 2022
vim.opt.number = false
-- vim.opt.mouse = 'a'
vim.opt.clipboard = 'unnamedplus'
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = true
vim.opt.hidden = true
vim.opt.showmatch = true
vim.opt.smartindent = true
vim.opt.wrap = true
vim.opt.breakindent = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.cmdheight = 2
vim.opt.scrolloff = 5
vim.opt.laststatus = 0 -- use 3 for global statusline
vim.opt.signcolumn = "yes:2"
vim.opt.list = truek

-- key bindings
-- Space as leader key
vim.g.mapleader = ' '

vim.keymap.set('i', 'jk', '<ESC>')

vim.keymap.set('n', '<leader>,', '<cmd>bp<CR>')
vim.keymap.set('n', '<leader>.', '<cmd>bn<CR>')

vim.keymap.set('n', '<leader><space>', '<cmd>noh<CR>')

-- commands

vim.api.nvim_create_user_command(
  'ReloadConfig',
  'source $MYVIMRC | PackerCompile',
  {}
)

-- packer
local install_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
local install_plugins = false

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  print('Installing packer...')
  local packer_url = 'https://github.com/wbthomason/packer.nvim'
  vim.fn.system({'git', 'clone', '--depth', '1', packer_url, install_path})
  print('Done.')

  vim.cmd('packadd packer.nvim')
  install_plugins = true
end

require('packer').startup(function(use)
  use { 'wbthomason/packer.nvim' }
  use { 'hashivim/vim-terraform' }
	use { "catppuccin/nvim", as = "catppuccin" }
  use {
    'numToStr/Comment.nvim',
    config = function()
        require('Comment').setup()
    end
  }
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  if install_plugins then
    require('packer').sync()
  end
end)

if install_plugins then
  print '=================================='
  print '    Plugins are being installed'
  print '    Wait until Packer completes,'
  print '       then restart nvim'
  print '=================================='
  return
end

-- telescop config

require("telescope").setup {
  defaults = {
    file_ignore_patterns = {
      "%.jpg",
      "%.jpeg",
      "%.png",
      "%.otf",
      "%.ttf",
      "node_modules/",
      ".git/",
    },
    prompt_prefix = "  ",
    selection_caret = "  ",
    entry_prefix = "  ",
    layout_strategy = "flex",
    layout_config = {
      horizontal = {
        preview_width = 0.6,
      },
    },
    border = {},
    borderchars = { " ", " ", " ", " ", " ", " ", " ", " " },
    -- borderchars = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
    -- borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
  },
  pickers = {
    find_files = {
      -- theme = "ivy",
      find_command = { "fd", "--type", "f", "-H" }
    },
  }
}

-- telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

-- Colorscheme

vim.opt.termguicolors = true

require("catppuccin").setup({
    flavour = "frappe", -- latte, frappe, macchiato, mocha
    background = { -- :h background
        light = "latte",
        dark = "frappe",
    },
    compile_path = vim.fn.stdpath("cache") .. "/catppuccin",
    transparent_background = false,
    term_colors = false,
    dim_inactive = {
        enabled = true,
        shade = "dark",
        percentage = 0.15,
    },
    styles = {
        comments = { "italic" },
        conditionals = { "italic" },
        loops = {},
        functions = { "italic" },
        keywords = {},
        strings = {},
        variables = {},
        numbers = { "bold" },
        booleans = {},
        properties = {},
        types = {},
        operators = {},
    },
    color_overrides = {},
    custom_highlights = {},
    integrations = {
        cmp = false,
        gitsigns = false,
        nvimtree = false,
        telescope = true,
        treesitter = false,
        -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
})

vim.api.nvim_command "colorscheme catppuccin"
