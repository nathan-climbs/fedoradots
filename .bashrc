# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

HISTCONTROL=ignoreboth:erasedups
HISTSIZE=1000
HISTFILESIZE=1000

# cool options for cool kids
shopt -s \
    autocd \
    globstar \
    checkwinsize \
    cdspell \
    dirspell \
    histappend \
    expand_aliases \
    dotglob \
    gnu_errfmt \
    histreedit \
    nocasematch

bind 'set show-all-if-ambiguous on'
bind 'set colored-stats on'
bind 'set completion-display-width 1'
bind 'TAB:menu-complete'

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# aliases
alias ls="ls -aFh --color=auto --group-directories-first"
alias ll="ls -lahF"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -I"
alias rmd="rm -rfI"
alias c="clear"

# eitors
alias e='~/bin/nvim.appimage'
alias vim='~/bin/nvim.appimage'

# git
alias gc='git clone'

# prompt
## Reset to normal: \033[0m
NORM="\033[0m"

## Colors:
RED="\033[0;31m"
CYN="\033[0;36m"
YEL="\033[0;33m"

#prompt
PS1="\n${YEL}\h ${CYN}\w ${RED}➜  ${NORM}"

#functions

# create a new directory and enter it
mkd() {
  mkdir -p "$1" && cd "$1"
}
